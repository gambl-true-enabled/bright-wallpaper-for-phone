package com.bright.wallpaper

import android.Manifest
import android.app.WallpaperManager
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Stack
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.setContent
import androidx.core.app.ActivityCompat
import com.bright.wallpaper.content.ContentControls
import com.bright.wallpaper.content.ContentPager
import com.bright.wallpaper.theme.BrightWallpaperForPhoneTheme
import com.bright.wallpaper.utils.SaveBitmapUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class WallpaperActivity : AppCompatActivity() {

    private val viewModel: WallpaperViewModel by viewModels()

    private val loadPermission = 123
    private val sharePermission = 124

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BrightWallpaperForPhoneTheme {
                Stack(
                        modifier = Modifier
                                .fillMaxSize()
                ) {
                    ContentPager(
                            modifier = Modifier,
                            stateModel = viewModel.stateModel
                    )
                    ContentControls(
                            modifier = Modifier
                                    .gravity(Alignment.BottomCenter),
                            stateModel = viewModel.stateModel,
                            setCallback = this@WallpaperActivity::setCurrentWallpaper,
                            loadCallback = this@WallpaperActivity::loadCurrentWallpaperWithPermission,
                            shareCallback = this@WallpaperActivity::shareCurrentWallpaperWithPermission
                    )
                }
            }
        }
    }

    private fun setCurrentWallpaper() {
        if (viewModel.isChanging) {
            Toast.makeText(this, "The change is in progress", Toast.LENGTH_SHORT).show()
            return
        }
        CoroutineScope(Dispatchers.IO).launch {
            viewModel.isChanging = true
            val bitmap = BitmapFactory.decodeResource(
                    resources,
                    viewModel.stateModel.imageData[viewModel.stateModel.selectedImageIndex.value].big
            )
            val wallpaperManager = WallpaperManager.getInstance(applicationContext)
            try {
                wallpaperManager.setBitmap(bitmap)
                withContext(Dispatchers.Main) {
                    Toast.makeText(this@WallpaperActivity, "Wallpaper changed successfully", Toast.LENGTH_SHORT).show()
                }
            } catch (e: IOException) {
                withContext(Dispatchers.Main) {
                    Toast.makeText(this@WallpaperActivity, "An error has occurred", Toast.LENGTH_SHORT).show()
                }
            } finally {
                viewModel.isChanging = false
            }
        }
    }

    private fun loadCurrentWallpaperWithPermission() {
        if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    loadPermission
            )
        } else
            loadCurrentWallpaper()
    }

    private fun shareCurrentWallpaperWithPermission() {
        if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    sharePermission
            )
        } else
            shareCurrentWallpaper()
    }

    private fun loadCurrentWallpaper(): String? {
        val bitmap = BitmapFactory.decodeResource(
                resources,
                viewModel.stateModel.imageData[viewModel.stateModel.selectedImageIndex.value].big
        )
        return SaveBitmapUtil.insertImage(
                contentResolver,
                bitmap,
                "Wallpaper" + System.currentTimeMillis().toString(),
                "Wallpaper image"
        )
    }

    private fun shareCurrentWallpaper() {
        val uriToImage = loadCurrentWallpaper() ?: return
        val shareIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, Uri.parse(uriToImage))
            type = "image/*"
        }
        startActivity(Intent.createChooser(shareIntent, "Share"))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            loadPermission -> {
                loadCurrentWallpaper()
            }
            sharePermission -> {
                shareCurrentWallpaper()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}