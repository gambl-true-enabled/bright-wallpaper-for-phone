package com.bright.wallpaper

import androidx.lifecycle.ViewModel
import com.bright.wallpaper.content.ImageModel
import com.bright.wallpaper.content.StateModel

class WallpaperViewModel: ViewModel() {

    var isChanging = false

    val stateModel = StateModel(
        imageData = listOf(
            ImageModel(
                small = R.drawable.ic_wallpaper_1_small,
                big = R.drawable.ic_wallpaper_1_big
            ),
            ImageModel(
                small = R.drawable.ic_wallpaper_2_small,
                big = R.drawable.ic_wallpaper_2_big
            ),
            ImageModel(
                small = R.drawable.ic_wallpaper_3_small,
                big = R.drawable.ic_wallpaper_3_big
            ),
            ImageModel(
                small = R.drawable.ic_wallpaper_4_small,
                big = R.drawable.ic_wallpaper_4_big
            ),
            ImageModel(
                small = R.drawable.ic_wallpaper_5_small,
                big = R.drawable.ic_wallpaper_5_big
            )
        )
    )
}