package com.bright.wallpaper.content

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource

@Composable
fun ContentPager(
    modifier: Modifier = Modifier,
    stateModel: StateModel = StateModel()
) {
    ViewPager(
        modifier = modifier
            .fillMaxSize(),
        index = stateModel.selectedImageIndex,
        range = IntRange(0, stateModel.imageData.lastIndex)
    ) {
        if (index < 0 || index > stateModel.imageData.lastIndex)
            return@ViewPager
        Image(
            modifier = modifier
                .fillMaxSize(),
            asset = imageResource(id = stateModel.imageData[index].big),
            contentScale = ContentScale.FillWidth
        )
    }
}