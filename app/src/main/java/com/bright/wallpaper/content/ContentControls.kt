package com.bright.wallpaper.content

import androidx.compose.animation.animate
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.RowScope.weight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bright.wallpaper.R

@Composable
fun ContentControls(
    modifier: Modifier = Modifier,
    stateModel: StateModel = StateModel(),
    setCallback: () -> Unit = {},
    loadCallback: () -> Unit = {},
    shareCallback: () -> Unit = {}
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
    ) {
        ContentButtons(
            modifier = Modifier.fillMaxWidth(),
            setCallback = setCallback,
            loadCallback = loadCallback,
            shareCallback = shareCallback,
        )
        ContentImages(
            modifier = Modifier.fillMaxWidth(),
            stateModel = stateModel
        )
    }
}

@Composable
private fun ContentImages(
    modifier: Modifier = Modifier,
    stateModel: StateModel = StateModel()
) {
    ScrollableRow(
        modifier = modifier
            .padding(top = 16.dp)
    ) {
        stateModel.imageData.forEachIndexed { index, image ->
            ImageWallpaper(
                image = image.small,
                isSelected = stateModel.selectedImageIndex.value == index,
                clickCallback = {
                    stateModel.selectedImageIndex.value = index
                }
            )
        }
    }
}

@Composable
private fun ImageWallpaper(
    modifier: Modifier = Modifier,
    isSelected: Boolean = false,
    image: Int = 0,
    clickCallback: () -> Unit = {}
) {
    Image(
        asset = imageResource(id = image),
        modifier = modifier
            .padding(animate(target =if (isSelected) 0.dp else 4.dp))
            .border(
                width = animate(target = if (isSelected) 4.dp else 0.dp),
                color = Color.Green
            )
            .clickable(onClick = clickCallback)
            .size(80.dp),
        contentScale = ContentScale.FillWidth
    )
}

@Composable
private fun ContentButtons(
    modifier: Modifier = Modifier,
    setCallback: () -> Unit = {},
    loadCallback: () -> Unit = {},
    shareCallback: () -> Unit = {}
) {
    val cornerRadius = 16.dp
    val shape1 = RoundedCornerShape(topLeft = cornerRadius, bottomLeft = cornerRadius)
    val shape2 = RoundedCornerShape(0.dp)
    val shape3 = RoundedCornerShape(topRight = cornerRadius, bottomRight = cornerRadius)
    Row(
        modifier = modifier
            .padding(4.dp)
    ) {
        ControlButton(
            modifier = Modifier
                .border(1.dp, Color.Green, shape1)
                .clip(shape1),
            text = R.string.load_wallp,
            clickCallback = loadCallback
        )
        ControlButton(
            modifier = Modifier
                .border(1.dp, Color.Green, shape2)
                .clip(shape2),
            text = R.string.set_wallp,
            clickCallback = setCallback
        )
        ControlButton(
            modifier = Modifier
                .border(1.dp, Color.Green, shape3)
                .clip(shape3),
            text = R.string.share_wallp,
            clickCallback = shareCallback
        )
    }
}

@Composable
private fun ControlButton(
    modifier: Modifier = Modifier,
    text: Int? = null,
    image: Int? = null,
    clickCallback: () -> Unit = {}
) {
    val modifierButton = Modifier
        .weight(1f)
        .padding(vertical = 8.dp)
    Surface(
        modifier = modifier
            .weight(1f)
            .clickable(onClick = clickCallback),
        color = Color.Transparent
    ) {
        when {
            text != null -> {
                Text(
                    text = stringResource(id = text),
                    modifier = modifierButton,
                    textAlign = TextAlign.Center,
                    fontSize = 21.sp,
                    color = Color.Green
                )
            }
            image != null -> {
                Image(
                    asset = imageResource(id = image),
                    modifier = modifierButton
                )
            }
        }
    }
}
