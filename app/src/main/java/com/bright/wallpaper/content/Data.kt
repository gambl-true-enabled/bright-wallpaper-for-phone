package com.bright.wallpaper.content

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.bright.wallpaper.R

data class StateModel(
    val selectedImageIndex: MutableState<Int> = mutableStateOf(0),
    val imageData: List<ImageModel> = emptyList()
)

data class ImageModel(
    val small: Int = 0,
    val big: Int = 0
)