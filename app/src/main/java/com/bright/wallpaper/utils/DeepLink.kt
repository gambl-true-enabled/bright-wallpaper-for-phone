package com.bright.wallpaper.utils

import android.content.Context

private const val WALLP_TABLE = "com.wallpaper.table.DEEP"
private const val WALLP_ARGS = "com.wallpaper.value.DEEP"

fun Context.link(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(WALLP_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(WALLP_ARGS, deepArgs).apply()
}

fun Context.args(): String? {
    val sharedPreferences = getSharedPreferences(WALLP_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(WALLP_ARGS, null)
}
